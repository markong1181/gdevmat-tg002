Mover mover;
void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.velocity = new PVector(1,0);
  //mover.acceleration = new PVector(0.1,0);
  
}

//PVector wind = new PVector(0.1f,0);
//PVector gravity = new PVector(0,-1);
void draw()
{
  background(255);
  mover.update();
  
  mover.render();
  mover.setColor(155, 25, 200, 255);
  if (mover.position.x > 0)
  {
    mover.velocity = new PVector(0,0);
  }
  //mover.applyForce(wind);
  //mover.applyForce(gravity);
  
  //if (mover.position.y < Window.bottom)
  //{
   // mover.velocity.y *= -1; // 3rd law of motion
   // mover.position.y = Window.bottom;
 // }
}
