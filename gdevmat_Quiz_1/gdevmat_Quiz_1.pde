
void setup() // initialization
{
  size(1080, 720, P3D); //P3D used to make things 3d
  background(255);
  camera(0,0,-(height/2.0f) / tan(PI * 30.0f /180.0f), // camera position
  0,0,0, // eye position
  0,-1,0); // up vector

}

void draw ()
{
  noStroke();
  fill(random(215), random(159), random(214), random(50));
  
  
  float mean = random(100);
  float std = 200;
  float gauss = randomGaussian();
  float sizeMean = random(10);
  float sizeStd = random(10);
  float sizeGauss = randomGaussian();
  
  
  float x = std * gauss + mean;
  float y = random(-500,500);
  float size = sizeMean * sizeStd + sizeGauss;
  
  circle(x,y,size);
}
