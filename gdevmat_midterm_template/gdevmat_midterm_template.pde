void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  movers = new Mover[100];
  initializeMovers();
}

Mover movers[];
Mover blackHole;

Mover mover = new Mover();

int frames = 0;

void draw()
{
  background(0);
  
  frames++;
  
  noStroke();
  
  if (frames >= 100)
  {
    frames = 0;
    initializeMovers();
  }
  
  
  
  for (int i = 0; i < 100; i++ )
  {
    movers[i].render();
    PVector dir = PVector.sub(blackHole.position, movers[i].position);
    dir.normalize().mult(15);
    movers[i].position.add(dir);
  }
  
  blackHole.render();
  
}

void initializeMovers()
{
  for (int i = 0; i < 100; i++)
   {
      float gauss = randomGaussian();
      float mean = random(Window.left, Window.right);
      float std = 50;
      float vertical = random(Window.bottom, Window.top);
    
      float x = std* gauss + mean;
      float y = std* gauss + vertical;
      
      
      movers[i] = new Mover(x, y, random(10, 50)); 
      movers[i].r = map(noise(random(255)), 0, 1, 0, 255);
      movers[i].g = map(noise(random(255)), 0, 1, 0, 255);
      movers[i].b = map(noise(random(255)), 0, 1, 0, 255);

   }
   
   blackHole = new Mover(random(Window.left, Window.right), 
                       random(Window.bottom, Window.top), 50); 
}
